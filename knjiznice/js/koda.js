
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var imagesLink = ["http://www2.pictures.gi.zimbio.com/AVN+Awards+Mandalay+Bay+Arrivals+tfDY_T2jc6yx.jpg", // Audrey
                    "http://www.nowgamer.com/wp-content/uploads/2014/07/380657.jpg",    //valeera
                    "http://4.bp.blogspot.com/-1fce-6COmPw/UFItqJepxRI/AAAAAAAAACE/qXhK9pENqSI/s1600/arthas_cover.jpg"];//arthas]//other
/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}

var weight = [];
var height = [];
var sistolic = [];
var distolic = [];
var temperature = [];
var birthDate = [];



function GeneratePatients() {
    generirajPodatke(1);
    generirajPodatke(2);
    generirajPodatke(3);
}
/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
 var sessionId;
 
function generirajPodatke(stPacienta) {
    
    sessionId = getSessionId();
    
    var ehrId = "";

    var Name,
        Surname,
        DateOfBirth,
        Gender,
        Height,
        Weight,
        Address,
        DataArray;
    // TODO: Potrebno implementirati
    switch (stPacienta) {
    case 1:
        Name = "Arthas";
        Surname = "Menethil";
        DateOfBirth = "1969-01-03T0:59";
        Height = 190;
        Weight = 90;
        //Address = " - ";
        Gender = "MALE";
        
        DataArray = [
            ["1970-03-04T00:01",  70, 40, 36.7, 120, 60],
            ["1980-08-08T08:08", 100, 55, 36.7, 120, 60],
            ["1980-08-08T08:08", 120, 55, 36.7, 120, 60],
            ["1980-08-08T08:08", 120, 55, 36.7, 120, 60],
            ["1980-08-08T08:08", 120, 55, 36.7, 120, 60],
            ["1980-08-08T08:08", 150, 55, 36.7, 120, 60],
            ["1980-08-08T08:08", 150, 55, 36.7, 120, 60],
            ["1990-11-11T11:12", 150, 60, 36.7, 120, 60],
            ["2000-12-12T12:12", 170, 70, 36.7, 120, 60],
            ["2015-04-01T00:00", 190, 90, 36.7, 120, 60],
            ["2015-04-01T00:00", 190, 90, 36.7, 120, 60]
            ]
        break;
    case 2:
        Name = "Audrey";
        Surname = "Hollander";
        DateOfBirth = "1979-11-04T4:20";
        Height = 168;
        Weight = 52;
        //Address = " - "
        Gender = "FEMALE";
        
        DataArray = [
            ["1970-03-04T00:01",  70, 40, 36.7, 120, 60],
            ["1980-08-08T08:08", 70 , 40, 36.7, 120, 60],
            ["1980-08-08T08:08", 100, 40, 36.7, 120, 60],
            ["1980-08-08T08:08", 100, 50, 36.7, 120, 60],
            ["1980-08-08T08:08", 110, 48, 36.7, 120, 60],
            ["1980-08-08T08:08", 111, 48, 36.7, 120, 60],
            ["1980-08-08T08:08", 130, 48, 36.7, 120, 60],
            ["1990-11-11T11:12", 130, 50, 36.7, 120, 60],
            ["2000-12-12T12:12", 168, 52, 36.7, 120, 60],
            ["2015-04-01T00:00", 168, 50, 36.7, 120, 60],
            ["2015-04-01T00:00", 168, 52, 36.7, 120, 60]
            ]
        break;
    case 3:
        Name = "Valeera";
        Surname = "Sanguinar";
        DateOfBirth = "1969-01-03T0:59";
        Height = 170;
        Weight = 55;
        //Address = " - "
        Gender = "FEMALE";
        
        DataArray = [
            ["1970-03-04T00:01",  70, 40, 36.7, 120, 60],
            ["1980-08-08T08:08", 90, 50, 36.7, 120, 60],
            ["1980-08-08T08:08", 90, 50, 36.7, 120, 60],
            ["1980-08-08T08:08", 90, 50, 36.7, 120, 60],
            ["1980-08-08T08:08", 100, 55, 36.7, 120, 60],
            ["1980-08-08T08:08", 111, 45, 36.7, 120, 60],
            ["1980-08-08T08:08", 130, 50, 36.7, 120, 60],
            ["1990-11-11T11:12", 150, 55, 36.7, 120, 60],
            ["2000-12-12T12:12", 150, 60, 36.7, 120, 60],
            ["2015-04-01T00:00", 170, 55, 36.7, 120, 60],
            ["2015-04-01T00:00", 170, 55, 36.7, 120, 60]
            ]
        break;
    }
    
    
    $.ajax({
    url: baseUrl + "/ehr",
    type: 'POST',
    headers: {"Ehr-Session": sessionId},
    success: function (data) {
        var ehrId = data.ehrId;

        // build party data
        var partyData = {
            firstNames:     Name,
            lastNames:      Surname,
            gender:         Gender,
            dateOfBirth:    DateOfBirth,
            //address:        [
             //               {key: "id",         value: "1"},                  404 Bad request from EHRScape
            //                {key: "version",    value: 0},
             //               {key: "address",    value: Address}],
            partyAdditionalInfo: [
                    {key: "ehrId",   value: ehrId},
                    {key: "weight",  value: Weight},
                    {key: "height",  value: Height},
            ]
        };
        $.ajax({
            url: baseUrl + "/demographics/party",
            type: 'POST',
            contentType: 'application/json',
            headers: {"Ehr-Session": sessionId},
            data: JSON.stringify(partyData),
            success: function (party) {
                if (party.action == 'CREATE') {
                    for(var i = 0; i < DataArray.length; i++){
                        AddData(ehrId,DataArray[i]);
                    }
                    switch (stPacienta) {
                    case 1:
                        $("#Arthas").val(ehrId);
                        break;
                    case 2:
                        $("#Audrey").val(ehrId);
                        break;
                    case 3:
                        $("#Valeera").val(ehrId);
                        break;
                    }
                }
            }
            });
        }
    });

}


function ShowData(Name, Surname, DOB, gender){
    var imagelink = "https://userscontent2.emaze.com/images/21541ce1-365a-4747-ac2a-c7cbae994bc8/93f46af51460d45523fc617b75b10993.png"
    $("#PatientInformation").html("<stong> Data of current patient:</strong>");
    $("#PatientName").html(Name+" "+ Surname);
    $("#PatientAge").html("Age: <strong>" + getAge(DOB) + "</strong>");
    $("#PatientDateOfBirth").html("Date of Birth: <strong>" + DOB + "</strong>");
    $("#PatientGender").html("Gender: <strong>" + gender.substring(0,1) + gender.substring(1,gender.length).toLowerCase() + "</strong>");
    $("#PatientWeight").html("Weight: <strong>"+ weight[10] +"<strong>");
    $("#PatientHeight").html("Height: <strong>"+ height[10] +"<strong>");
    $("#PatientBMI").html("BMI: <strong>"+ Math.floor(BMI()) +"<strong>");
    $("#PatientDetailInfo").html("<strong>Detailed Info:<strong>");
    
    
    //$("#PatientAddress").html("Address: <strong>" + Address + "</strong>");  couldnt get the value for it somehow
    if(Name == "Arthas"){
        imagelink = imagesLink[2];
    }
    if(Name == "Audrey"){
        imagelink = imagesLink[0];
    }
    if(Name == "Valeera"){
        imagelink = imagesLink[1];
    }
    $("#PatientImage").css('background-image', 'url('+imagelink+')');
 
}

// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
function AddData(ehrId, Data) {
    sessionId = getSessionId();
    
    $.ajaxSetup({
        headers: {
            "Ehr-Session": sessionId
        }
    });
    var compositionData = {
    "ctx/time": Data[0],
    "ctx/language": "en",
    "ctx/territory": "CA",
    "vital_signs/body_temperature/any_event/temperature|magnitude": Data[3],
    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
    "vital_signs/blood_pressure/any_event/systolic": Data[4],
    "vital_signs/blood_pressure/any_event/diastolic": Data[5],
    "vital_signs/height_length/any_event/body_height_length": Data[1],
    "vital_signs/body_weight/any_event/body_weight": Data[2]
    };
    var queryParams = {
    "ehrId": ehrId,
    templateId: 'Vital Signs',
    format: 'FLAT',
    committer: 'Sasha'
    };
    
    $.ajax({
    url: baseUrl + "/composition?" + $.param(queryParams),
    type: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(compositionData),
    success: function (res) {},
    });
    
    
    
    
}


function getAge(DOB){
    var str = DOB.substr(0,4);
    var currentYear = 2017;
    var DateofBirth = parseInt(str);
    return currentYear - DateofBirth;
}

function gogetEHRId(id){
    var ehrId = ""
    switch(id){
        case 1:
            ehrId = $("#Arthas").val();
            break;
        case 2:
            ehrId = $("#Valeera").val();
            break;
        case 3:
            ehrId = $("#Audrey").val();
            break;
        case 4:
            ehrId = $("#YourOwn").val();
            break;
    }
    
    getEHRId(ehrId);
    getAllData(ehrId);
    
    
}




function getEHRId(ehrId){
    
    sessionId = getSessionId();
    
    $("#PatientInformation").html("");
    $("#PatientInformation").val("");
    $("#PatientName").html("");
    $("#PatientName").val("");
    $("#PatientAge").html("");
    $("#PatientAge").val("");
    $("#PatientDateOfBirth").html("");
    $("#PatientDateOfBirth").val("");
    $("#PatientGender").html("");
    $("#PatientGender").val("");
    $("#PatientAddress").html("");
    $("#PatientAddress").val("");
    $("#PatientHeight").val("");
    $("#PatientHeight").html("");
    $("#PatientWeight").val("");
    $("#PatientWeight").html("");
    $("#PatientBMI").val("");
    $("#PatientBMI").html("");
    
    $.ajax({
      url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
      type: 'GET',
      headers: {
        "Ehr-Session": sessionId
      },
      success: function(data) {
        var party = data.party;
        ShowData(data.party.firstNames, data.party.lastNames, data.party.dateOfBirth, data.party.gender)
      },
    });
    
}

function getAllData(ehrId){
    sessionId = getSessionId();
      // get height
      $.ajax({
        url: baseUrl + "/view/" + ehrId + "/height",
        type: 'GET',
        headers: {"Ehr-Session": sessionId },
        success: function(data) {
            for (var i = 0; i < 10; i++) {
              height[10 - i] = data[i].height;
              console.log("1")
            }
            GraphItHeight();
          
        },
      }),
      //get weight
      $.ajax({
        url: baseUrl + "/view/" + ehrId + "/weight",
        type: 'GET',
        headers: {
          "Ehr-Session": sessionId
        },
        success: function(data) {
            for (var i = 0; i < 10; i++) {
              
              weight[10 - i] =  data[i].weight
              birthDate[10 - i] =  data[i].time
            }
            //graph Weight
            GraphItWeight();
          },
        }),
        $.ajax({
        url: baseUrl + "/view/" + ehrId + "/body_temperature",
        type: 'GET',
        headers: {
          "Ehr-Session": sessionId
        },
        success: function(data) {
            for (var i = 0; i < 10; i++) {
              
              temperature[10 - i] =  data[i].body_temperature
              birthDate[10 - i] =  data[i].time
            }
            // Graph Temp
            GraphItTemperature();
          },
        })
}


function GraphItWeight(){
    $("#weight").css('height', '300px');
    $("#weight").css('width', '300px');
  var weighty = {
    x: birthDate,
    y: weight,
    name: 'Weight (kg)',
    type: 'bar'
  };

  var data = [weighty];
  var layout = {
    xaxis: {
      title: 'Time'
    },
    yaxis: {
      title: 'Value'
    },
    barmode: 'absolute',
    title: 'Weight Measurements',
  };

  Plotly.newPlot('weight', data, layout);
}
function GraphItHeight(){
    $("#height").css('height', '300px');
    $("#height").css('width', '300px');
  var heighty = {
    x: birthDate,
    y: height,
    name: 'Height (cm)',
    type: 'bar'
  };

  var data = [heighty];
  var layout = {
    xaxis: {
      title: 'Time'
    },
    yaxis: {
      title: 'Value'
    },
    barmode: 'absolute',
    title: 'Height Measurements',
  };

  Plotly.newPlot('height', data, layout);
}
function GraphItTemperature(){
    $("#temperature").css('height', '300px');
    $("#temperature").css('width', '300px');
  var temperatury = {
    x: birthDate,
    y: temperature,
    name: 'Temperature (C)',
    type: 'bar'
  };

  var data = [temperatury];
  var layout = {
    xaxis: {
      title: 'Time'
    },
    yaxis: {
      title: 'Value'
    },
    barmode: 'absolute',
    title: 'Temperature Measurements',
  };

  Plotly.newPlot('temperature', data, layout);
}


function BMI(){
    
    var finalBmi = weight[10]/(height[10]/100*height[10]/100)
    return finalBmi;
}